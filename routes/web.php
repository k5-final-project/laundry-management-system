<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/biodata', function () {
    return view('biodata.biodata');
});
Route::get('/', function () {
    return view('auth.login');
});
Route::group(['middleware' => ['auth']], function () {
    
    Route::get('/dashboard', [DashboardController::class,'index']);
    Route::get('/package', [PackageController::class,'index']);
    Route::get('/package/new', [PackageController::class,'create']);
    Route::post('/package/save', [PackageController::class,'save']);
    Route::get('/package/show/{id}', [PackageController::class,'show']);
    Route::put('/package/edit/{id}', [PackageController::class,'edit']);
    Route::delete('/package/remove/{id}', [PackageController::class,'remove']);
    
    Route::get('/customer', [CustomerController::class,'index']);
    Route::get('/customer/new', [CustomerController::class,'create']);
    Route::post('/customer/save', [CustomerController::class,'save']);
    Route::get('/customer/show/{id}', [CustomerController::class,'show']);
    Route::get('/customer/export', [CustomerController::class,'export']);
    Route::put('/customer/edit/{id}', [CustomerController::class,'edit']);
    Route::delete('/customer/remove/{id}', [CustomerController::class,'remove']);

    
    
    Route::get('/transaction/list-transaction', [TransactionController::class,'list']);
    Route::get('/transaction', [TransactionController::class,'index']);
    Route::get('/transaction/view/{id}', [TransactionController::class,'view']);
    Route::get('/transaction/update_status/{id}', [TransactionController::class,'update_status']);
    Route::put('/transaction/update_payment', [TransactionController::class,'update_payment']);
    Route::post('/transaction/submit', [TransactionController::class,'store']);
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
