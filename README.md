## Final Project

### Kelompok 5

### Anggota Kelompok

- Arsika Citra Pelangi [Telegram](https://t.me/chikapelangi)
- Rosmalinda Marbun [Telegram](https://t.me/skm_lindamarbun)
- Edward Bona Franscoist [Telegram](https://t.me/edw_pakpahan)

### Tema Project

Sistem Manajemen Laundry

### ERD

<p align="center"><img src="/ERD_K5_Laundry.png" width="400" alt="Entity Relationship Diagram"></p>


### Link Video

 - [Link Demo Aplikasi](https://drive.google.com/file/d/1PUq1hYd-5T7rSlrNfeH0pEWNGpDNyVK_/view?usp=sharing)
