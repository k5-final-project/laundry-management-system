@extends('layout.master')
@section('title', 'List Package')

@section('content')
@include('sweetalert::alert')
<a href="package/new" class="btn btn-success">New Package</a>
<table class="table" id="tbl-cust">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>

        <th scope="col">Price</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @php
          $i=1;
      @endphp
      @foreach ($packages as $package)
          
      <tr>
        <th scope="row">{{$i++}}</th>
        <td>{{$package->name}}</td>
  
        <td>{{$package->price}}</td>
        <td>
          <a href="/package/show/{{$package->id}}" class="btn btn-primary" style="margin-right: 10px;"><i class="fa fa-edit"></i></a>
          <form action="/package/remove/{{$package->id}}" method="POST">
            @csrf
            @method('delete')
            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
        </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  
 
@endsection
@push('scripts')
<script src="{{asset('/template/js/plugins/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/template/js/plugins/dataTables/dataTables.bootstrap.js')}}"></script>

<script>
    
$( document ).ready(function() {
    $("#tbl-cust").DataTable();

});

</script>
@endpush
@push('styles')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush