@extends('layout.master')
@section('title', 'New Form Package')

@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Data package
        </div>
        <form action="/package/save" method="POST" enctype="multipart/form-data">
            @csrf
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="">Name</label>
                        <input type="text" class="form-control" " name="name">
                    </div>
                    @error('name')
                    <p class="help-block">{{ $message }}</p>
                  @enderror
                </div>
               
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('weight') has-error @enderror">
                        <label for="">Weight</label>
                        <input type="number" class="form-control"  name="weight">
                        @error('weight')
                        <p class="help-block">{{ $message }}</p>
                      @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('price') has-error @enderror">
                        <label for="">Price</label>
                        <input type="text" name="price" class="form-control">
                        @error('price')
                        <p class="help-block">{{ $message }}</p>
                      @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection