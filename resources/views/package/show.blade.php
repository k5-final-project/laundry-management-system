@extends('layout.master')
@section('title', 'Form Package')

@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Package
        </div>
        <form action="/package/edit/{{$package->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="">Name</label>
                        <input type="text" class="form-control" value="{{$package->name}}" name="name">
                    </div>
                    @error('name')
                    <p class="help-block">{{ $message }}</p>
                  @enderror
                </div>
               
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('weight') has-error @enderror">
                        <label for="">Weight</label>
                        <input type="number" class="form-control" value="{{$package->weight}}" name="weight">
                        @error('weight')
                        <p class="help-block">{{ $message }}</p>
                      @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('price') has-error @enderror">
                        <label for="">price</label>
                        <input class="form-control" id="price" name="price" rows="3" value="{{$package->price}}"/>
                        @error('price')
                        <p class="help-block">{{ $message }}</p>
                      @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <a href="/package" class="btn btn-secondar">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection