@extends('layout.master')
@section('title', 'Data Transaction')

@section('content')
@include('sweetalert::alert')
<div class="col-lg">
    
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="list-transaction">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer</th>
                            <th>Status</th>
                            <th>Payment</th>
                            <th>Date Create</th>
                            <th>Last Update</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @php
                            $i=1;
                        @endphp
                        @foreach ($transactions as $trx)
                            
                        <tr>
                            <td>{{$i++;}}</td>
                            <td>{{$trx->customer->name }}</td>
                            <td> @if ($trx->logs->isNotEmpty())
                                <span class="@if ($trx->logs->first()->description == 'In Progress') bg-info @else bg-success @endif" style="padding: 7px; border-radius: 10px;">
                                    {{$trx->logs->first()->description}}
                                </span>
                            @else
                                No log found
                            @endif</span></td>
                            <td>@if ($trx->is_paid == 1)
                                <span class="bg-success" style="color:green;padding:7px; border-radius:10px;">Paid</span>
                            @else
                            <span class="bg-danger" style="color:red;padding:7px; border-radius:10px;">Unpaid</span>
                            @endif</td>
                            <td>{{$trx->created_date}}</td>
                            <td>{{$trx->updated_at}}</td>
                            <td>
                                <a href="/transaction/update_status/{{$trx->id}}" style="margin-right: 5px" ><i class="fa fa-check"></i></a>
                                @if ($trx->is_paid == 0)
                                <a href="#" data-toggle="modal" onclick="update_payment({{$trx->id}})" data-target="#exampleModal" style="margin-right: 5px" data-id="{{$trx->id}}" ><i class="fa fa-money"></i></a>
                                @endif
                                <a href="/transaction/view/{{$trx->id}}"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                            
    </form>
      </div>
    </div>
  </div>
</div>
                        @endforeach
                        
                    </tbody>
                </table>
               
            </div>
            <!-- /.table-responsive -->
   <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Payment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/transaction/update_payment" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
        <div class="modal-body">
          <div class="form-group">
            <label for="photo">Payment Photo</label>
            <input type="file" name="photo">
          </div>
          <input type="hidden" name="id_trx" id="id_trx">
          <div class="form-group">
            <label>Payment Method</label>
            <select class="form-control" name="payment_method">
                <option value="cash">Cash</option>
                <option value="debit">Debit</option>
                <option value="qris">QRIS</option>
            
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
@endsection
@push('scripts')
<script src="{{asset('/template/js/plugins/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/template/js/plugins/dataTables/dataTables.bootstrap.js')}}"></script>
<script>
$( document ).ready(function() {
    $("#list-transaction").DataTable();

  });
  function update_payment(id){
    $("#id_trx").val(id);
  }
</script>
  
@endpush
@push('styles')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush