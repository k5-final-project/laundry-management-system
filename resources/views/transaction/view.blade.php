@extends('layout.master')
@section('title', 'View Transaction')

@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Form Order Customer
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <form role="form">
                        <div class="form-group">
                            <label>Name <span class="text-danger">*</span></label>
                            <select class="form-control" id="name" name="name" disabled> 
                       
                                @foreach ($customers as $customer)
                                    
                                <option value="{{$customer->id}}" @if ($customer->id == $transaction->customer_id) selected @endif >{{$customer->name}}</option>
                                @endforeach
                            </select>
                           
                        </div>
                        <div class="form-group">
                            <label>Package Laundry <span class="text-danger">*</span></label>
                            <select class="form-control" id="packageLaundry" name="package" disabled> 
                                
                                <option value="" selected disabled>- Choose One -</option>
                                @foreach ($packages as $package)
                                <option value="{{$package->id}}" @if ($package->id == $transaction->package_id) selected @endif>{{$package->name}}</option>
                                @endforeach
                            </select>
                           
                        </div>
                        <div class="form-group">
                            <label>Payment Status <span class="text-danger">*</span></label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="is_paid" id="optionsRadiospaid" class="radiopayment" value="1" @if ($transaction->is_paid == 1) checked @endif>Paid
                                </label>
                       
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" class="radiopayment" name="is_paid" id="optionsRadiosunpaid" value="0" @if ($transaction->is_paid == 0) checked @endif>Unpaid
                                </label>
                            </div>
                           
                        </div>

               
                     
                  
                </div>
                <!-- /.col-lg-6 (nested) -->
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Phone Number <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" placeholder="Enter number" name="phone_number" id="phone_number"  disabled>
                       
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Weight <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" placeholder="Enter number" name="weight" id="weight" disabled value="{{$transaction->weight}}">
                                <p class="help-block" id="txtprice"></p>
                             
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label>Price <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" placeholder="Enter number" name="price" id="price" value="{{$transaction->total_price}}"readonly>
                              
                            </div>
                        </div>
                    </div>
                   @if ($transaction->is_paid == 1)
                       
                   <div class="form-group" id="payment_method" >
                       <label>Payment Method</label>
                       <select class="form-control" name="payment_method" disabled>
                           <option value="cash" @if ($trx_payment->paymentInfo->payment_method == 'cash') selected @endif>Cash</option>
                           <option value="debit" @if ($trx_payment->paymentInfo->payment_method == 'debit') selected @endif>Debit</option>
                           <option value="qris" @if ($trx_payment->paymentInfo->payment_method == 'qris') selected @endif>QRIS</option>
                       
                       </select>
                      
                   </div>
                   @endif
                    <div class="text-right">

                        <a href="/transaction/list-transaction" class="btn btn-primary">Back</a>
                    </div>
                </div>
           
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->
        </div>
         </form>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
@endsection
@push('scripts')

<script>
$( document ).ready(function() {
    const array_cust = @json($customers ?? []); 
    const array_package = @json($packages ?? []); 


  </script>
    
  @endpush
