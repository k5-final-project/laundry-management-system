@extends('layout.master')
@section('title', 'New Transaction')

@section('content')
@include('sweetalert::alert')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Form Order Customer
        </div>
        <form action="/transaction/submit" method="POST" enctype="multipart/form-data">
            @csrf
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <form role="form">
                        <div class="form-group @error('name') has-error @enderror">
                            <label>Name <span class="text-danger">*</span></label>
                            <select class="form-control" id="name" name="name"> 
                                <option value="" selected disabled>- Choose One -</option>
                                @foreach ($customers as $customer)
                                    
                                <option value="{{$customer->id}}">{{$customer->name}}</option>
                                @endforeach
                            </select>
                            @error('name')
                            <p class="help-block">{{ $message }}</p>
                          @enderror
                        </div>
                        <div class="form-group @error('package') has-error @enderror">
                            <label>Package Laundry <span class="text-danger">*</span></label>
                            <select class="form-control" id="packageLaundry" name="package"> 
                                
                                <option value="" selected disabled>- Choose One -</option>
                                @foreach ($packages as $package)
                                <option value="{{$package->id}}">{{$package->name}}</option>
                                @endforeach
                            </select>
                            @error('package')
                            <p class="help-block">{{ $message }}</p>
                          @enderror
                        </div>
                        <div class="form-group @error('is_paid') has-error @enderror">
                            <label>Payment Status <span class="text-danger">*</span></label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="is_paid" id="optionsRadiospaid" class="radiopayment" value="1" >Paid
                                </label>
                       
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" class="radiopayment" name="is_paid" id="optionsRadiosunpaid" value="0" >Unpaid
                                </label>
                            </div>
                            @error('is_paid')
                            <p class="help-block">{{ $message }}</p>
                        @enderror
                        </div>

               
                     
                  
                </div>
                <!-- /.col-lg-6 (nested) -->
                <div class="col-lg-6">
                    <div class="form-group @error('phone_number') has-error @enderror">
                        <label>Phone Number <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" placeholder="Enter number" name="phone_number" id="phone_number"  disabled>
                        @error('phone_number')
                        <p class="help-block">{{ $message }}</p>
                    @enderror
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group @error('weight') has-error @enderror">
                                <label>Weight <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" placeholder="Enter number" name="weight" id="weight" disabled>
                                <p class="help-block" id="txtprice"></p>
                                @error('weight')
                                <p class="help-block">{{ $message }}</p>
                            @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @error('price') has-error @enderror">
                                <label>Price <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" placeholder="Enter number" name="price" id="price" readonly>
                                @error('price')
                                <p class="help-block">{{ $message }}</p>
                            @enderror
                            </div>
                        </div>
                    </div>
                   
                    <div class="form-group @error('payment_method') has-error @enderror" id="payment_method" style="display:none;">
                        <label>Payment Method</label>
                        <select class="form-control" name="payment_method">
                            <option value="cash">Cash</option>
                            <option value="debit">Debit</option>
                            <option value="qris">QRIS</option>
                        
                        </select>
                        @error('payment_method')
                        <p class="help-block">{{ $message }}</p>
                    @enderror
                    </div>
                    <div class="form-group @error('photo') has-error @enderror" id="photo" style="display: none;">
                        <label>Payment Photo</label>
                        <input type="file" name="photo">
                        @error('photo')
                        <p class="help-block">{{ $message }}</p>
                    @enderror
                    </div>
                   <div class="text-right">

                       <button type="submit" class="btn btn-success">Submit</button>
                   </div>
                </div>
            </form>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->
        </div>
         </form>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
@endsection
@push('scripts')

<script>
$( document ).ready(function() {
    const array_cust = @json($customers ?? []); 
    const array_package = @json($packages ?? []); 

$("#optionsRadiospaid").on('click', function(){
    $("#payment_method").css('display','block')
    $("#photo").css('display','block')
})
$("#optionsRadiosunpaid").on('click', function(){
    $("#payment_method").css('display','none')
    $("#photo").css('display','none')
})
$("#packageLaundry").on('change', function(){
    let pck_id = $(this).find(":selected").val();
  
  const data = array_package.find(function(package) {
       return pck_id == package.id;
   });
   if (data) {
    $("#txtprice").html('Rp. '+data.price+'/kg')
   }
   $('#weight').prop('disabled',false)
  
})
$("#weight").on('change', function(){
    let weight = $(this).val();
    let pck_id = $('#packageLaundry').find(":selected").val();
  
  const data = array_package.find(function(package) {
       return pck_id == package.id;
   });
   if (data) {
   
    $("#txtprice").html('Rp. '+data.price+'/kg')
    $("#price").val(data.price * weight)
   }
  
})
$("#name").on('change', function(){
   let cust_id = $(this).find(":selected").val();
  
   const data = array_cust.find(function(customer) {
        return cust_id == customer.id;
    });
    if (data) {
        $("#phone_number").val(data.phone_number);
    }else{
        $("#phone_number").val('');

    }
 
})
});

  </script>
    
  @endpush
