<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Laundry Lab</title>

    <!-- Core CSS - Include with every page -->
    <link href="{{asset('/template/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/template/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->
    <link href="{{asset('/template/css/sb-admin.css')}}" rel="stylesheet">
    @stack('styles')
</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            @include('partials.navbar')
            @include('partials.sidebar')

        

          
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">@yield('title')</h3>
                </div>
                <div class="col-lg-12">
                    <div class="">
                     
                        <div class="">
                            @yield('content')
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="{{asset('/template/js/jquery-1.10.2.js')}}"></script>
    <script src="{{asset('/template/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/template/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>

    <!-- Page-Level Plugin Scripts - Blank -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="{{asset('/template/js/sb-admin.js')}}"></script>

    <!-- Page-Level Demo Scripts - Blank - Use for reference -->
   
    @stack('scripts')
      
</body>

</html>
