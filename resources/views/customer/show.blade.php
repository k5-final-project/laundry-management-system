@extends('layout.master')
@section('title', 'Form Customer')

@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Customer
        </div>
        <form action="/customer/edit/{{$customer->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="">Name</label>
                        <input type="text" class="form-control" value="{{$customer->name}}" name="name">
                    </div>
                    @error('name')
                    <p class="help-block">{{ $message }}</p>
                  @enderror
                </div>
               
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('phone_number') has-error @enderror">
                        <label for="">Phone Number</label>
                        <input type="number" class="form-control" value="{{$customer->phone_number}}" name="phone_number">
                        @error('phone_number')
                        <p class="help-block">{{ $message }}</p>
                      @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('address') has-error @enderror">
                        <label for="">Address</label>
                        <textarea class="form-control" id="address" name="address" rows="3" >{{$customer->address}}</textarea>
                        @error('address')
                        <p class="help-block">{{ $message }}</p>
                      @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <a href="/customer" class="btn btn-secondar">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection