
@extends('layout.master')
@section('title', 'New Form Customer')

@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Customer
        </div>
        <form action="/customer/save" method="POST" enctype="multipart/form-data">
            @csrf
            @include('sweetalert::alert')
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="">Name</label>
                        <input type="text" class="form-control" " name="name">
                    </div>
                    @error('name')
                    <p class="help-block">{{ $message }}</p>
                  @enderror
                </div>
               
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('phone_number') has-error @enderror">
                        <label for="">Phone Number</label>
                        <input type="number" class="form-control"  name="phone_number">
                        @error('phone_number')
                        <p class="help-block">{{ $message }}</p>
                      @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @error('address') has-error @enderror">
                        <label for="">Address</label>
                        <textarea class="form-control" id="address" name="address" rows="3" ></textarea>
                        @error('address')
                        <p class="help-block">{{ $message }}</p>
                      @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection