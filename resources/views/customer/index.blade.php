
@extends('layout.master')
@section('title', 'List Customer')

@section('content')
@include('sweetalert::alert')
<a href="customer/new" class="btn btn-success">New Customer</a>
<a href="customer/export" class="btn btn-warning">Export Customer</a>
<table class="table" id="tbl-cust">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>

        <th scope="col">Phone</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @php
          $i=1;
      @endphp
      @foreach ($customers as $customer)
          
      <tr>
        <th scope="row">{{$i++}}</th>
        <td>{{$customer->name}}</td>
  
        <td>{{$customer->phone_number}}</td>
        <td>
          <a href="/customer/show/{{$customer->id}}" class="btn btn-primary" style="margin-right: 10px;"><i class="fa fa-edit"></i></a>
          <form action="/customer/remove/{{$customer->id}}" method="POST">
            @csrf
            @method('delete')
            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
        </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  
 
@endsection
@push('scripts')
<script src="{{asset('/template/js/plugins/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/template/js/plugins/dataTables/dataTables.bootstrap.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
     @if(Session::has('success'))
        Swal.fire({
            title: 'Success',
            text: '{{ Session::get('success') }}',
            icon: 'success',
            confirmButtonText: 'OK'
        });
    @endif
$( document ).ready(function() {
    $("#tbl-cust").DataTable();

});

</script>
@endpush
@push('styles')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush