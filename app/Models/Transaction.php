<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Log_transaction;
use App\Models\Payment_info;
use App\Models\Customer;
class Transaction extends Model
{
    use HasFactory;
    protected $table = 'transaction';
    protected $fillable = ['weight','total_price','is_paid','customer_id','package_id','created_date'];
    

    public function logs()
    {
        return $this->belongsToMany(Log_transaction::class, 'transaction_log', 'transaction_id', 'log_trx_id')
            ->withTimestamps();
    }
    public function paymentInfo()
    {
        return $this->hasOne(Payment_info::class);
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
