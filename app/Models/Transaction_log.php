<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction_log extends Model
{
    use HasFactory;
    protected $table = 'transaction_log';

    protected $fillable = [
        'transaction_id',
        'logs_id',
    ];
}
