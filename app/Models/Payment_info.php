<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment_info extends Model
{
    use HasFactory;
    protected $table = 'payment_info';

    protected $fillable = [
        'payment_method',
        'photo',
        'payment_date',
        'transaction_id'
    ];
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
