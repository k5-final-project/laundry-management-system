<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log_transaction extends Model
{
    use HasFactory;
    protected $table = 'log_trx';

    protected $fillable = [
        'description',
    ];
}
