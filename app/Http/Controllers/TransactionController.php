<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Log_transaction;
use App\Models\Payment_info;
use App\Models\Customer;
use App\Models\Package;
use Illuminate\Support\Facades\DB;
use Alert;
class TransactionController extends Controller
{
    public function index(){
        $customers = Customer::all();
        $packages = Package::all();
        
        return view('transaction.index',['customers'=>$customers,'packages'=> $packages]);
    }
    public function view($id){
        $data = Transaction::find($id);
        $customers = Customer::all();
        $packages = Package::all();
        $trx_payment = Transaction::with('paymentInfo')
        ->where('id', $id)
        ->first();
        return view('transaction.view',['transaction'=>$data,'trx_payment'=>$trx_payment,'customers'=>$customers,'packages'=> $packages]);
    }
    public function store(Request $request)
    {
 
   
        $request->validate([
            'name' => 'required',
            'package' => 'required',
            'weight' => 'required',
            'is_paid' => 'required',
           'payment_method' => $request->is_paid == "1" ? 'required' : '',
           'price' => 'required',
        
        ]);
  
        if ($request->is_paid == "1" ) {
            $request->validate([
                'photo' => 'required|mimes:jpg,png,JPG,PNG',
            ]);
        
        }
       
        $trx = new Transaction;
 
        $trx->customer_id = $request->name;
        $trx->package_id = $request->package;
        $trx->weight = $request->weight;
        $trx->is_paid = $request->is_paid;
        $trx->total_price = $request->price;
        $trx->created_date = date("Y-m-d H:i:s");
     
        $trx->save();
        if($request->is_paid == "1"){
            $photo_name = time().'.'.$request->file('photo')->extension();
            $request->file('photo')->move(public_path('image'),$photo_name);
            $paymentInfo = $trx->paymentInfo()->create([
                'transaction_id' => $trx->id,
                'payment_method' => $request->payment_method,
                'photo' => $photo_name,
                'payment_date' => date('Y-m-d H:i:s')
            ]);
          

            $log_trx = new Log_transaction(['description' => 'Payment Success']);
            $log_trx->save();
            $trx->logs()->attach($log_trx);
        }

        $log_trx = new Log_transaction(['description' => 'In Progress']);
        $log_trx->save();
        $trx->logs()->attach($log_trx);
        Alert::success('Success', 'New transaction successfully added!');
        return redirect('/transaction/list-transaction');
    }
    public function list(){
        
        $transactions = Transaction::with(['logs' => function ($query) {
            $query->where('description', '!=', 'Payment Success')
                  ->orderByDesc('id');
        }, 'customer'])
        ->orderByDesc('id')
        ->get();
       
       
       
        return view('transaction.list', ['transactions'=> $transactions]);
    }
    public function update_status($id){
        $transaction = Transaction::find($id);
        if (!$transaction) {
            return redirect('/transaction/list-transaction');
        }
     
        
 
        $log_trx = new Log_transaction(['description' => 'Finish']);
        $log_trx->save();
        $transaction->logs()->attach($log_trx);
        Alert::success('Success', 'Status successfully changed!');
         return redirect('/transaction/list-transaction');
    }
    public function update_payment(Request $request){

        $transaction = Transaction::find($request->id_trx);
        $payment = Payment_info::where('transaction_id',$request->id_trx)->first();
        if($payment){
            return redirect('/transaction/list-transaction');
        }
        $request->validate([
            'photo' => 'required|mimes:jpg,png,JPG,PNG',
            'payment_method' => 'required'
        ]);

       
        $photo_name = time().'.'.$request->file('photo')->extension();
        $request->file('photo')->move(public_path('image'),$photo_name);
        $paymentInfo = $transaction->paymentInfo()->create([
            'transaction_id' => $transaction->id,
            'payment_method' => $request->payment_method,
            'photo' => $photo_name,
            'payment_date' => date('Y-m-d H:i:s')
        ]);
     

        $log_trx = new Log_transaction(['description' => 'Payment Success']);
        $log_trx->save();
        $transaction->logs()->attach($log_trx);
        
        $transaction->is_paid = 1;
        $transaction->save();
        Alert::success('Success', 'Payment successfully updated!');
        return redirect('/transaction/list-transaction');
    }
}
