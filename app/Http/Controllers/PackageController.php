<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Transaction;
use App\Models\Package;
use Alert;
class PackageController extends Controller
{
    public function index(){
      
        $packages = Package::all();
        return view('package.index', ['packages' => $packages]);
    }
    public function create(){
      
        return view('package.create');
        
    }
    public function save(Request $request){
        $request->validate([
            'name' => 'required|min:3',
            'weight' => 'required|min:1',
            'price' => 'required',
        ]);
       
        $package = Package::where('name', $request->name)->exists();;
      
        if($package){
            Alert::error('Error', 'New package failed added!');
            return redirect('/package');
        }else{
            $package = new Package();
            $package->name = $request->name;
            $package->weight = $request->weight;
            $package->price = $request->price;
            $package->save();
            Alert::success('Success', 'New package successfully added!');
        }
        return redirect('/package');
        
    }
    public function show($id){
        $package = Package::find($id);
      
        return view('package.show', ['package' => $package]);
    }
    public function edit($id, Request $request){
        
        $request->validate([
            'name' => 'required|min:3',
            'weight' => 'required|min:1',
            'price' => 'required',
        ]);
        
        $package = Package::find($id);
       
        if ($package) {
            $transactions = Transaction::where('package_id', $id)->exists();
            if ($transactions) {
                Alert::error('Error', 'Package failed edited!');
                return redirect('/package');
            } else {
                $package->name = $request->name;
                $package->weight = $request->weight;
                $package->price = $request->price;
                $package->save();
                Alert::success('Success', 'New package successfully edited!');
                return redirect('/package');
            }
        }else{
            Alert::error('Error', 'package not found!');
            return redirect('/package');
        }
        $package->save();
        return redirect('/package');
        
    
    }
    public function remove($id){
        $package = Package::find($id);
       
        if ($package) {
            $transactions = Transaction::where('package_id', $id)->exists();
    
            if ($transactions) {
                Alert::error('Failed', 'Failed removed package!');
                return redirect('/package');
            } else {
                $package->delete();
                Alert::success('Success', 'Package successfully removed!');
                return redirect('/package');
            }
        } else {
            Alert::error('Error', 'package not found!');
            return redirect('/package');
        }
    }
}
