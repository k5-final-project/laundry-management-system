<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Transaction;
use Alert;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportCustomers;

class CustomerController extends Controller
{
    public function index(){
        $customers = Customer::all();
        return view('customer.index', ['customers' => $customers]);
    }
    public function create(){
      
        return view('customer.create');
        
    }
  
    public function save(Request $request){
        $request->validate([
            'name' => 'required|min:3',
            'phone_number' => 'required|min:10',
            'address' => 'required',
        ]);
       
        $customer = Customer::where('phone_number', $request->phone_number)->exists();;
      
        if($customer){
            Alert::error('Error', 'New customer failed added!');
            return redirect('/customer');
        }else{
            $customer = new Customer();
            $customer->name = $request->name;
            $customer->address = $request->address;
            $customer->phone_number = $request->phone_number;
            $customer->save();
            Alert::success('Success', 'New customer successfully added!');
        }
        return redirect('/customer');
    }
    public function show($id){
        $customer = Customer::find($id);
      
        return view('customer.show', ['customer' => $customer]);
    }
    public function edit($id, Request $request){
        
        $request->validate([
            'name' => 'required|min:3',
            'phone_number' => 'required|min:10',
            'address' => 'required',
        ]);
        
        $customer = Customer::find($id);
        $customer->name = $request->name;
        $customer->phone_number = $request->phone_number;
        $customer->address = $request->address;

        $customer->save();
        Alert::success('Success', 'Customer successfully edited!');
        return redirect('/customer');
        
    
    }
    public function remove($id){
        $customer = Customer::find($id);
       
        if ($customer) {
            $transactions = Transaction::where('customer_id', $id)->exists();
    
            if ($transactions) {
                Alert::error('Error', 'Customer failed removed!');
                return redirect('/customer');
            } else {
                $customer->delete();
                Alert::success('Success', 'Customer successfully removed!');
                return redirect('/customer');
            }
        } else {
            Alert::error('Error', 'customer not found!');
            return redirect('/customer');
        }
    }
    public function export(){
        return Excel::download(new ExportCustomers, 'listcustomer.xlsx');
    }
}
